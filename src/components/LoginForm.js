import styles from "./LoginForm.module.css";

export default function LoginForm() {
  return (
    <form className={styles.form}>
      <div>
        <label className={styles.labelStyle}>Name</label>
        <input className={styles.inputStyle} />
      </div>
      <div>
        <label className={styles.labelStyle}>Password</label>
        <input className={styles.inputStyle} />
      </div>
      <div>
        <button className={styles.submitStyles} type="submit">
          Submit
        </button>
      </div>
    </form>
  );
}
